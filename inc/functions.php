<?php

/**
 * Gets the last modified date of the CSS/JS file
 */
if ( ! function_exists( 'engzaFileVersion' ) ) {
  function engzaFileVersion($ver) {
    date_default_timezone_set( 'America/Chicago' );
    $themePath = ENGZA_PLUGIN_PATH . '/';
    return date( 'Y.m.d.Hi', filemtime( $themePath . $ver ) );
  }
}

/**
 * Load "js/admin.min.js" on all admin pages
 */
if ( ! function_exists( 'engza_admin_script' ) ) {
  function engza_admin_script() {
    $mainScriptPath = ENGZA_PLUGIN_ROOT_URL . 'js/admin.min.js';
    $scriptPages = array(
      'wp-plugin-boilerplate',
      'wp-plugin-boilerplate-add',
      'wp-plugin-boilerplate-edit',
      'wp-plugin-boilerplate-delete',
      'wp-plugin-boilerplate-deleted',
      'wp-plugin-boilerplate-settings'
    );
    if ( isset( $_GET['page'] ) && in_array( $_GET['page'], $scriptPages ) ) {
      wp_enqueue_media(); // JS files don't seem to load without this function
      wp_register_script( 'engza-admin', $mainScriptPath, array( 'jquery' ), engzaFileVersion( 'js/admin.min.js' ), true );
      wp_enqueue_script( 'engza-admin' );
    }
  }
  add_action( 'admin_enqueue_scripts', 'engza_admin_script' );
}

/**
 * Load "css/admin.min.css" on all admin pages
 */
if ( ! function_exists( 'engza_admin_style' ) ) {
  function engza_admin_style() {
    $mainStylePath = ENGZA_PLUGIN_ROOT_URL . 'css/admin.min.css';
    $stylePages = array(
      'wp-plugin-boilerplate',
      'wp-plugin-boilerplate-add',
      'wp-plugin-boilerplate-edit',
      'wp-plugin-boilerplate-delete',
      'wp-plugin-boilerplate-deleted',
      'wp-plugin-boilerplate-settings'
    );
    if ( isset( $_GET['page'] ) && in_array( $_GET['page'], $stylePages ) ) {
      wp_register_style( 'engza-admin', $mainStylePath, false, engzaFileVersion( 'css/admin.min.css' ) );
      wp_enqueue_style( 'engza-admin' );
    }
  }
  add_action( 'admin_enqueue_scripts', 'engza_admin_style' );
}
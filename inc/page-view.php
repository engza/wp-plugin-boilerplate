<?php

if ( ! function_exists( 'engza_page_view' ) ) {

  function engza_page_view() {

    // check user capabilities
    if ( ! current_user_can( 'manage_options' ) ) {
      wp_die( __( 'You do not have sufficient permissions to access this page.', ENGZA_PLUGIN_DATA['TextDomain'] ) );
    }

    if (isset($_GET['page'])) {
      if ( $_GET['page'] == 'wp-plugin-boilerplate' ) {

        // Set variables for the main landing page
        $engza_page_h1 = 'Boilerplate Landing Page';
        global $wpdb;
        $engza_get_all_results = $wpdb->get_results( " SELECT * FROM " . ENGZA_DB_TABLE_NAME . " " );

      } elseif ( $_GET['page'] == 'wp-plugin-boilerplate-deleted' ) {

        // Set variables for the deleted landing page
        $engza_page_h1 = 'Boilerplate Deleted Page';
        global $wpdb;
        $engza_get_all_results = $wpdb->get_results( " SELECT * FROM " . ENGZA_DB_TABLE_NAME . " WHERE `deleted` = '1' " );

      } else {
        wp_die( __( 'You do not have sufficient permissions to access this page.', ENGZA_PLUGIN_DATA['TextDomain'] ) );
      }
    } else {
      wp_die( __( 'You do not have sufficient permissions to access this page.', ENGZA_PLUGIN_DATA['TextDomain'] ) );
    }

    ?>

    <div class="wrap">
      <h1><?php echo $engza_page_h1; ?></h1>
    </div>

    <?php if ( $engza_get_all_results ) : ?>

      <table class="wp-list-table widefat fixed striped" id="table_engzaPageView">
        <thead>
          <tr>
            <th scope="col" id="engza_column_one" class="manage-column column-one column-primary">One</th>
            <th scope="col" id="engza_column_two" class="manage-column column-two">Two</th>
            <th scope="col" id="engza_column_three" class="manage-column column-three">Three</th>
            <th scope="col" id="engza_column_four" class="manage-column column-four">Four</th>
          </tr>
        </thead>
        <tbody>

        <?php

          foreach ( $engza_get_all_results as $engza_result_detail ) {

            $engza_db_id              = $engza_result_detail->id ? $engza_result_detail->id : '' ;
            $engza_db_updatedDate     = $engza_result_detail->updated_date ? date("M. j, Y g:ia", substr($engza_result_detail->updated_date, 0, 10)) : '';
            $engza_db_shortDesciption = $engza_result_detail->short_description ? html_entity_decode(stripslashes($engza_result_detail->short_description)) : '';
            $engza_db_textarea        = $engza_result_detail->textarea ? html_entity_decode(stripslashes(wpautop($engza_result_detail->textarea))) : '';
            $engza_edit_URL           = ENGZA_ADMIN_URL . '-edit&id=' . $engza_db_id;
            $engza_delete_URL         = ENGZA_ADMIN_URL . '-delete&id=' . $engza_db_id;

            ?>

            <tr id="id-<?php echo $engza_db_id; ?>">
              <td class="one column-one has-row-actions column-primary" data-colname="One">
                <?php echo $engza_db_updatedDate; ?>
                <div class="row-actions">
                  <span class="edit"><a href="<?php echo $engza_edit_URL; ?>">Edit</a> | </span>
                  <span class="custom"><a style="color:#317D38" href="#">Custom</a> | </span>
                  <span class="delete"><a href="<?php echo $engza_delete_URL; ?>">Delete</a></span>
                </div>
                <button type="button" class="toggle-row">
                  <span class="screen-reader-text">Show more details</span>
                </button>
              </td>
              <td class="date column-two" data-colname="Two">
                <?php echo $engza_db_id; ?>
              </td>
              <td class="story column-three" data-colname="Three">
                <?php echo $engza_db_shortDesciption; ?>
              </td>
              <td class="story column-four" data-colname="Four">
                <?php echo $engza_db_textarea; ?>
              </td>
            </tr>

            <?php

          }

        ?>

        </tbody>
        <tfoot>
          <tr>
            <th scope="col" class="manage-column column-one column-primary">One</th>
            <th scope="col" class="manage-column column-two">Two</th>
            <th scope="col" class="manage-column column-three">Three</th>
            <th scope="col" class="manage-column column-four">Four</th>
          </tr>
        </tfoot>
      </table>

      <?php else: ?>

        <h2>Sorry, there is no data to display.</h2>

      <?php endif;

  }

}
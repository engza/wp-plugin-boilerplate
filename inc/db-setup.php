<?php

/**
 * Create the database table if it doesn't exist
 *
 *    bigint(20) is used by wp_users for the ID field, so that's what we'll use
 *    for this table's "id" field
 */
if ($wpdb->get_var("SHOW TABLES LIKE '".ENGZA_DB_TABLE_NAME."'") != ENGZA_DB_TABLE_NAME) {
  $charset_collate = $wpdb->get_charset_collate();
  $sql = "
    CREATE TABLE ".ENGZA_DB_TABLE_NAME." (
      `id`                 bigint(20)   NOT NULL AUTO_INCREMENT,
      `created_date`       int(20)      NOT NULL COMMENT 'Date entry was created',
      `created_user`       bigint(20)   NOT NULL COMMENT 'User ID of who created the entry',
      `updated_date`       int(20)               COMMENT 'Date entry was last updated',
      `updated_user`       bigint(20)            COMMENT 'User ID of who last updated the entry',
      `textarea`           text         NOT NULL COMMENT 'Textarea for entry',
      `short_description`  varchar(255)          COMMENT 'Short description for entry',
      `deleted`            int(1)       NOT NULL DEFAULT '0' COMMENT '0 = not deleted, 1 = deleted',
      PRIMARY KEY (`id`)
    ) $charset_collate;
  ";
  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($sql);
}

/**
 * Use the following snippet (if necessary) for adding a column to the database AFTER implementation in a production environment
 */
// $imageColumn = $wpdb->get_results(  "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'ENGZA_DB_TABLE_NAME' AND column_name = 'image'"  );
// if(empty($imageColumn)){
//    $wpdb->query("ALTER TABLE ENGZA_DB_TABLE_NAME ADD `image` int(20) COMMENT 'Image ID from Media Library'");
// }
<?php

if ( ! function_exists( 'engza_add_admin_menus' ) ) {

  function engza_add_admin_menus() {

    // if capability is the same for all pages, set capability here
    $engza_plugin_cap = 'manage_options';

    // Slug/URL for the landing page of the plugin (/wp-admin/admin.php?page=wp-plugin-boilerplate)
    $engza_plugin_mainSlugURL = 'wp-plugin-boilerplate';

    // Function name used for the HTML in /inc/page-view.php
    $engza_plugin_viewFunction = 'engza_page_view';

    // Function name used for the HTML in /inc/page-action.php
    $engza_plugin_actionFunction = 'engza_page_action';

    // Function name used for the HTML in /inc/page-action.php
    $engza_plugin_settingsFunction = 'engza_page_settings';

    /**
     * Create a main-level menu item
     * Purpose: Landing page
     */
    add_menu_page(
      __( 'Main Plugin Page', ENGZA_PLUGIN_DATA['TextDomain'] ), // Page Title
      __( 'Main Plugin Page', ENGZA_PLUGIN_DATA['TextDomain'] ), // Menu Title
      $engza_plugin_cap,                                         // Capability
      $engza_plugin_mainSlugURL,                                 // Menu Slug (URL)
      $engza_plugin_viewFunction,                                // Function @ /inc/page-view.php
      'dashicons-admin-generic'                                  // Icon
    );

    /**
     * Create a submenu item under the main-level menu item
     * Purpose: Change display name of first sub-menu item
     * @link https://wordpress.stackexchange.com/questions/66498/add-menu-page-with-different-name-for-first-submenu-item
     */
    add_submenu_page(
      $engza_plugin_mainSlugURL,                                   // Parent Slug
      __( 'First Submenu Item', ENGZA_PLUGIN_DATA['TextDomain'] ), // Page Title
      __( 'First Submenu Item', ENGZA_PLUGIN_DATA['TextDomain'] ), // Menu Title
      $engza_plugin_cap,                                           // Capability
      $engza_plugin_mainSlugURL                                    // Menu Slug (URL)
    );

    /**
     * Create a submenu item under the main-level menu item
     * Purpose: Add [something] page (CRUD example)
     */
    add_submenu_page(
      $engza_plugin_mainSlugURL,                        // Parent Slug
      __( 'Add New', ENGZA_PLUGIN_DATA['TextDomain'] ), // Page Title
      __( 'Add New', ENGZA_PLUGIN_DATA['TextDomain'] ), // Menu Title
      $engza_plugin_cap,                                // Capability
      $engza_plugin_mainSlugURL.'-add',                 // Menu Slug (URL)
      $engza_plugin_actionFunction                      // Function @ /inc/page-action.php
    );

    /**
     * Create a submenu item under the submenu page
     * Hidden since menus don't list 3rd level menu items
     * Purpose: Edit [something] page - hidden - accepts URL parameter (CRUD example)
     */
    add_submenu_page(
      $engza_plugin_mainSlugURL.'-add',              // Parent Slug
      __( 'Edit', ENGZA_PLUGIN_DATA['TextDomain'] ), // Page Title
      __( 'Edit', ENGZA_PLUGIN_DATA['TextDomain'] ), // Menu Title
      $engza_plugin_cap,                             // Capability
      $engza_plugin_mainSlugURL.'-edit',             // Menu Slug (URL)
      $engza_plugin_actionFunction                   // Function @ /inc/page-action.php
    );

    /**
     * Create a submenu item under the submenu page
     * Hidden since menus don't list 3rd level menu items
     * Purpose: Delete [something] page - hidden - accepts URL parameter (CRUD example)
     */
    add_submenu_page(
      $engza_plugin_mainSlugURL.'-add',                // Parent Slug
      __( 'Delete', ENGZA_PLUGIN_DATA['TextDomain'] ), // Page Title
      __( 'Delete', ENGZA_PLUGIN_DATA['TextDomain'] ), // Menu Title
      $engza_plugin_cap,                               // Capability
      $engza_plugin_mainSlugURL.'-delete',             // Menu Slug (URL)
      $engza_plugin_actionFunction                     // Function @ /inc/page-action.php
    );

    /**
     * Create a submenu item under the submenu page
     * Hidden since menus don't list 3rd level menu items
     * Purpose: Deleted page - hidden - optional (CRUD example)
     */
    add_submenu_page(
      $engza_plugin_mainSlugURL.'-add',                 // Parent Slug
      __( 'Deleted', ENGZA_PLUGIN_DATA['TextDomain'] ), // Page Title
      __( 'Deleted', ENGZA_PLUGIN_DATA['TextDomain'] ), // Menu Title
      $engza_plugin_cap,                                // Capability
      $engza_plugin_mainSlugURL.'-deleted',             // Menu Slug (URL)
      $engza_plugin_viewFunction                        // Function @ /inc/page-view.php
    );

    /**
     * Create a submenu item under the main-level menu item
     * Purpose: Manage options/settings for the plugin
     */
    add_submenu_page(
      $engza_plugin_mainSlugURL,                         // Parent Slug
      __( 'Settings', ENGZA_PLUGIN_DATA['TextDomain'] ), // Page Title
      __( 'Settings', ENGZA_PLUGIN_DATA['TextDomain'] ), // Menu Title
      $engza_plugin_cap,                                 // Capability
      $engza_plugin_mainSlugURL.'-settings',             // Menu Slug (URL)
      $engza_plugin_settingsFunction                     // Function @ /inc/page-settings.php
    );

  }

}
add_action( 'admin_menu', 'engza_add_admin_menus' );
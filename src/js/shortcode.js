/**
 * If this file is going to be called in the FOOTER (preferred),
 * use the following anonymous function for jQuery and delete the function below.
 * @link https://digwp.com/2011/09/using-instead-of-jquery-in-wordpress/
 */
(function($) {

  // Code here

})( jQuery );

/**
 * If this file is going to be called in the HEADER,
 * use the following anonymous function for jQuery and delete the function above.
 * @link https://digwp.com/2011/09/using-instead-of-jquery-in-wordpress/
 */
 jQuery(document).ready(function( $ ) {

  // Code here

});
<?php
/**
 * Plugin Name: WP Plugin Boilerplater
 * Plugin URI: https://gitlab.com/engza/wp-plugin-boilerplate#readme
 * Description: Boilerplate for a custom WordPress plugin.
 * Version: 1.0.0
 * Author: Zach Engstrom
 * Author URI: https://gitlab.com/engza/
 * Text Domain: wp-plugin-boilerplate
 *
 *   ___ _ __   __ _ ______ _
 *  / _ \ '_ \ / _` |_  / _` |
 * |  __/ | | | (_| |/ / (_| |
 *  \___|_| |_|\__, /___\__,_|
 *             |___/
 */

if ( ! defined( 'ABSPATH' ) ) wp_die( 'No direct access allowed' );

require_once dirname( __FILE__ ) . '/constants.php';
require_once ENGZA_PLUGIN_PATH   . '/inc/functions.php';
require_once ENGZA_PLUGIN_PATH   . '/inc/db-setup.php';
require_once ENGZA_PLUGIN_PATH   . '/inc/add-menus.php';
require_once ENGZA_PLUGIN_PATH   . '/inc/page-view.php';
require_once ENGZA_PLUGIN_PATH   . '/inc/page-action.php';
require_once ENGZA_PLUGIN_PATH   . '/inc/shortcode.php';
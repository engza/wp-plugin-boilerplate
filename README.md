## ![WordPress Logo](../img/wordpress-logo_icon.png) Plugin Boilerplate

### Example File Structure

```
[prefix]-custom-plugin-name/
 ├── css/
 │    ├── admin.min.css
 │    ├── index.php
 │    └── shortcode.min.css
 ├── img/
 │    └── index.php
 ├── inc/
 │    ├── add-menus.php
 │    ├── db-setup.php
 │    ├── functions.php
 │    ├── index.php
 │    ├── page-action.php
 │    ├── page-view.php
 │    └── shortcode.php
 ├── js/
 │    ├── admin.min.js
 │    ├── index.php
 │    └── shortcode.min.js
 ├── src/
 │    ├── js/
 │    │    ├── admin.js
 │    │    └── shortcode.js
 │    └── sass/
 │         ├── admin.scss
 │         └── shortcode.scss
 ├── .gitignore
 ├── [prefix]-custom-plugin-name.php
 ├── constants.php
 ├── gulpfile.js
 ├── index.php
 ├── package.json
 └── README.md
```
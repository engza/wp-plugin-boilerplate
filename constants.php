<?php

// Set timezone for plugin to use
date_default_timezone_set( 'America/Chicago' );

// .../wp-content/plugins/wp-plugin-boilerplate (directory pathing)
define( 'ENGZA_PLUGIN_PATH', dirname( __FILE__ ) );

// .../wp-content/plugins/wp-plugin-boilerplate (URL pathing)
define( 'ENGZA_PLUGIN_ROOT_URL', plugins_url() . '/wp-plugin-boilerplate' );

// .../wp-admin/admin.php?page=wp-plugin-boilerplate
define( 'ENGZA_ADMIN_URL', get_site_url() . '/wp-admin/admin.php?page=wp-plugin-boilerplate' );

// gets URL of current page
define( 'ENGZA_PLUGIN_FORM_SELF', ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http' ) . '://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]' );

// Set database table name
global $wpdb;
define( 'ENGZA_DB_TABLE_NAME' , $wpdb->prefix . 'engza_wppluginboilerplate' );

// Get plugin data (set in core php file comments)
define( 'ENGZA_PLUGIN_DATA', get_plugin_data( ENGZA_PLUGIN_PATH . '/wp-plugin-boilerplate.php' ) );
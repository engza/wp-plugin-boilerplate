const gulp          = require('gulp'),
      sass          = require('gulp-sass'),
      sourcemaps    = require('gulp-sourcemaps'),
      autoprefixer  = require('gulp-autoprefixer'),
      rename        = require('gulp-rename'),
      concat        = require('gulp-concat'),
      uglify        = require('gulp-uglify'),
      zipit         = require('gulp-zip');

/**
 * Add leading zero to number if it's less than 10
 */
function leadingZero(val) {
  val = val < 10 ? "0" + val : val;
  return val;
}

/**
 * Get current date information
 */
var today = new Date,
    epoch = today.getTime(),
    s = leadingZero(today.getSeconds()),
    i = leadingZero(today.getMinutes()),
    H = leadingZero(today.getHours()),
    d = leadingZero(today.getDate()),
    m = leadingZero(today.getMonth() + 1),
    Y = leadingZero(today.getFullYear()),
    today = Y + m + d + "-" + H + i + s; // Ex: 20190125-162416

/**
 * Define Paths
 *
 * Pattern Examples:
 *    - sassFs = sass files
 *    - jsDir  = js directory
 *    - zipFN  = zip filename
 */
var path = {
  src: {
    dir: {
      sassRoot: 'src/sass/',
    },
    files: {
      sass: 'src/sass/**.scss',
      js: [
        'src/js/**.*',
        '!src/js/*.min.*',
      ],
      zipIn: [
        '**/*',
        '!node_modules{,/**}',
        '!bundled{,/**}',
        '!src{,/**}',
        '!.gitignore',
        '!gulpfile.js',
        '!package.json',
        '!package-lock.json',
      ],
    },
  },
  dest: {
    dir: {
      css: 'css/',
      js: 'js/',
      pretty: 'src/sass/pretty/',
      zipOut: 'bundled/'+today+'/',
    },
    files: {
      zipOut: 'wp-plugin-boilerplate.zip',
    },
  },
  gulpWatch: {
    files: {
      sass: 'src/sass/**/*.scss',
      js: 'src/js/**/*.js',
    }
  },
};

function styles() {
  return gulp.src(path.src.files.sass)
  .pipe(sourcemaps.init())
  .pipe(sass({errLogToConsole: true,outputStyle: 'expanded'}))
  .pipe(autoprefixer({browsers: ['last 2 versions', '> 5%', 'Firefox ESR']}))
  .pipe(gulp.dest(path.dest.dir.pretty))
  .pipe(sass({errLogToConsole: true,outputStyle: 'compressed'}))
  .pipe(rename({ suffix: '.min' }))
  .pipe(sourcemaps.write('./', {
    includeContent: false,
    sourceRoot: path.src.dir.sassRoot
  }))
  .pipe(gulp.dest(path.dest.dir.css));
}

function scripts() {
  return gulp.src(path.src.files.js, { sourcemaps: false })
    .pipe(uglify({
      output: {
        comments: /^\!/ // keep comments that start with "/*!"
      }
    }))
    .pipe(rename({ suffix: '.min' }))
    //.pipe(concat('main.min.js'))
    .pipe(gulp.dest(path.dest.dir.js));
}

function zip() {
 return gulp.src(path.src.files.zipIn)
 .pipe(zipit(path.dest.files.zipOut))
 .pipe(gulp.dest(path.dest.dir.zipOut));
}

function watch() {
  gulp.watch(path.gulpWatch.files.js, scripts);
  gulp.watch(path.gulpWatch.files.sass, styles);
}

var build = gulp.parallel(styles, scripts);

exports.styles = styles;
exports.scripts = scripts;
exports.zip = zip;
exports.watch = watch;
exports.build = build;

exports.default = build;